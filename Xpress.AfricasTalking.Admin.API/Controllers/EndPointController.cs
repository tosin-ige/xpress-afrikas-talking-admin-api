﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Xpress.AfricasTalking.Admin.API.Model;

namespace Xpress.AfricasTalking.Admin.API.Controllers
{
    [Route("api/EndPoint")]
    [ApiController]
    public class EndPointController : ControllerBase
    {
        private readonly SMSAdminDBContext _context;

        public EndPointController(SMSAdminDBContext context)
        {
            _context = context;
        }

        // GET: api/EndPoint
        [HttpGet("list")]
        public async Task<ActionResult<IEnumerable<TableEndPoint>>> GetTableEndPoint()
        {
            return await _context.TableEndPoint.ToListAsync();
        }

        // GET: api/EndPoint/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TableEndPoint>> GetTableEndPoint(int id)
        {
            
            var tableEndPoint = await _context.TableEndPoint.FindAsync(id);

            if (tableEndPoint == null)
            {
                return NotFound();
            }

            return tableEndPoint;
        }

        // PUT: api/EndPoint/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        [Obsolete]
        public IActionResult PutTableEndPoint(int id, EndPointModel model)
        {
            string resp = "";
            try
            {
                Random rnd = new Random();
                int num = rnd.Next(1, 2345698);
                if (model.status == "Active")
                {
                    _context.Database.ExecuteSqlCommand("UPDATE Table_EndPoint set Status='Dormant'; UPDATE Table_EndPoint SET EndPoint='" + model.endpoint + "',provider='" + model.provider + "',status='" + model.status + "',Last_Modified=GETDATE() WHERE Id='" + id + "'");
                    resp = "Update was successful";
                }
                else
                {
                    _context.Database.ExecuteSqlCommand("UPDATE Table_EndPoint SET EndPoint='" + model.endpoint + "',provider='" + model.provider + "',status='" + model.status + "',Last_Modified=GETDATE() WHERE Id='" + id + "'");
                    resp = "Update was successful";
                }
            }
            catch (DbUpdateException ex)
            {
                resp = ex.Message;
            }

            return Ok(resp);
        }

        // POST: api/EndPoint
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost("Add")]
        [Obsolete]
        public IActionResult PostTableEndPoint([FromBody]EndPointModel model)
        {
            string resp = "";
            try
            {
                Random rnd = new Random();                
                int num = rnd.Next(1, 2345698);
                _context.Database.ExecuteSqlCommand("INSERT INTO Table_EndPoint(Id,EndPoint,Provider,Status,Date_Added,Last_Modified)VALUES('" + num + "','" + model.endpoint + "','" + model.provider + "','" + model.status + "',GETDATE(),GETDATE())");
                resp = "Registration was successful";
            }
            catch (DbUpdateException ex)
            {
                resp = ex.Message;
            }

            return Ok(resp);
        }

        // DELETE: api/EndPoint/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TableEndPoint>> DeleteTableEndPoint(int id)
        {
            var tableEndPoint = await _context.TableEndPoint.FindAsync(id);
            if (tableEndPoint == null)
            {
                return NotFound();
            }

            _context.TableEndPoint.Remove(tableEndPoint);
            await _context.SaveChangesAsync();

            return tableEndPoint;
        }

        private bool TableEndPointExists(int id)
        {
            return _context.TableEndPoint.Any(e => e.Id == id);
        }
    }
}
