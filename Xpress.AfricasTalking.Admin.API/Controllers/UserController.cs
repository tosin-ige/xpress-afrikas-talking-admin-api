﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Xpress.AfricasTalking.Admin.API.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Cors;

namespace Xpress.AfricasTalking.Admin.API.Controllers
{
    [Route("api/User")]
    // [EnableCors]
   // [EnableCors("AllowOrigin")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly SMSAdminDBContext _context;

        public UserController(SMSAdminDBContext context)
        {
            _context = context;
        }

        // GET: api/User
        [HttpGet("list")]
        public IEnumerable<TableUser> GetTableUser()
        {
            //  return db.Table_User.ToList();
            var blogs =  _context.TableUser.ToList(); //_context.TableUser.("SELECT * FROM Table_User").ToList();
            return blogs;
        }
        /* public async Task<ActionResult<IEnumerable<TableUser>>> GetTableUser()
         {
             return await _context.TableUser.ToListAsync();
         }*/

        // GET: api/User/5
        [HttpGet("{id}")]
        public ActionResult<TableUser> GetTableUser(int id)
        {
            var tableUser = _context.TableUser.Find(id);

            if (tableUser == null)
            {
                return NotFound();
            }

            return tableUser;
        }

        // PUT: api/User/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        [Obsolete]
        public IActionResult PutTableUser(int id, UserModel model)
        {
            string resp = "";
            try
            {
                // _context.Database.ExecuteSqlCommand("INSERT INTO Table_EndPoint(Id,EndPoint,Provider,Status,Date_Added,Last_Modified)VALUES('" + num + "','" + model.endpoint + "','" + model.provider + "','" + model.status + "',GETDATE(),GETDATE())");
                _context.Database.ExecuteSqlCommand("UPDATE Table_User SET Surname='" + model.surname + "',othename='" + model.othername + "',email='" + model.email + "',phoneno='" + model.phoneno + "',Date_Modified=GETDATE() WHERE Id='" + id + "'");
                resp = "Update was successful";
            }
            catch (DbUpdateException ex)
            {
                resp = ex.Message;
            }

            return Ok(resp);
        }

        // POST: api/User
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost("Add")]
        [Obsolete]
        public IActionResult Add([FromBody]UserModel model)
        {
            string resp = "";
            var req = new TableUser
            {
                Surname = model.surname,
                OtheName = model.othername,
                Email = model.email,
                PhoneNo = model.phoneno,
                UserName = model.username,
                Password = model.password
            };

                try
                {
                    Random rnd = new Random();
                    int num = rnd.Next(1, 2345698);
                    _context.Database.ExecuteSqlCommand("INSERT INTO Table_User(Id,Surname,Othename,Email,PhoneNo,UserName,Password,Date_Added,Date_Modified)VALUES('" + num + "','" + model.surname + "','" + model.othername + "','" + model.email + "','" + model.phoneno + "','" + model.username + "','" + model.password + "',GETDATE(),GETDATE())");
                    resp = "Registration was successful";
                }
                catch (DbUpdateException ex)
                {
                    resp = ex.Message;
                }
            return Ok(resp);
        }

        // DELETE: api/User/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TableUser>> DeleteTableUser(int id)
        {
            var tableUser = await _context.TableUser.FindAsync(id);
            if (tableUser == null)
            {
                return NotFound();
            }

            _context.TableUser.Remove(tableUser);
            await _context.SaveChangesAsync();

            return tableUser;
        }

        [Route("auth")]
        [HttpPost]
        public string Post(UserModel model)
        {
             string res = "";
             var myUser = _context.TableUser.FirstOrDefault(u => u.UserName == model.username && u.Password == model.password);

             if (myUser == null)    //User was found
             {
                 res = "Incorrect Username or Password!";
             }
             else
             {
                HttpContext.Session.SetString("mysession", model.username);
                 res = "Success";
             }
             return res;
        }

        [Route("changePassword")]
        [HttpPost]
        [Obsolete]
        public string changePassword(ChangePasswordModel model)
        {
            string res = "";
            var myUser = _context.TableUser.FirstOrDefault(u => u.UserName == model.userName && u.Password == model.oldPassword);

            if (myUser == null)    //User was found
            {
                res = "Current Password is Incorrect for this User!";
            }
            else
            {
                //HttpContext.Session.SetString("mysession", model.username);
                _context.Database.ExecuteSqlCommand("UPDATE Table_User SET Password='" + model.newPassword + "',Date_Modified=GETDATE() WHERE UserName='" + model.userName + "'");
                res = "Password successfully changed!";
            }
            return res;
        }

        [Route("logout")]
        [HttpPost]
        [Obsolete]
        public string logout()
        {
            string res = "";
            try
            {
                HttpContext.Session.GetString("mysession");
                res = "true";
            }
            catch (Exception ex)
            {
                res = ex.Message;
            }
            return res;
        }

        [Route("authenticate")]
        [HttpPost]
        public bool authenticate()
        {
             bool resp = false;
             if (HttpContext.Session.GetString("mysession") != null)
             {
                 resp = true;
             }
             else
             {
                 resp = false;
             }
             return resp;
           // return HttpContext.Session.GetString("mysession");
        }

        private bool TableUserExists(int id)
        {
            return _context.TableUser.Any(e => e.Id == id);
        }
    }
}
