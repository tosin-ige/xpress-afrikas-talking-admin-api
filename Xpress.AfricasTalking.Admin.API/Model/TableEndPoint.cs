﻿using System;
using System.Collections.Generic;

namespace Xpress.AfricasTalking.Admin.API.Model
{
    public partial class TableEndPoint
    {
        public int Id { get; set; }
        public string EndPoint { get; set; }
        public string Provider { get; set; }
        public string Status { get; set; }
        public string DateAdded { get; set; }
        public string LastModified { get; set; }
    }
}
