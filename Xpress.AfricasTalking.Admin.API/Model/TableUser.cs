﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Xpress.AfricasTalking.Admin.API.Model
{
    public partial class TableUser
    {
        [Key]
        public int Id { get; set; }
        public string Surname { get; set; }
        public string OtheName { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string DateAdded { get; set; }
        public string DateModified { get; set; }
    }
}
