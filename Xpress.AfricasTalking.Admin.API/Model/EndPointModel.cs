﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Xpress.AfricasTalking.Admin.API.Model
{
    public class EndPointModel
    {
        public string endpoint { get; set; }
        public string provider { get; set; }
        public string status { get; set; }
    }
}
