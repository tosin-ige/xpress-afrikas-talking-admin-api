﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Xpress.AfricasTalking.Admin.API.Model
{
    public partial class SMSAdminDBContext : DbContext
    {
       /* public SMSAdminDBContext()
        {
        }*/

        public SMSAdminDBContext(DbContextOptions<SMSAdminDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TableEndPoint> TableEndPoint { get; set; }
        public virtual DbSet<TableUser> TableUser { get; set; }

       /* protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=(localdb)\\mssqllocaldb;Initial Catalog=SMSAdminDB;MultipleActiveResultSets=true;");
            }
        }*/

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TableEndPoint>(entity =>
            {
                entity.ToTable("Table_EndPoint");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.DateAdded).HasColumnName("Date_Added");

                entity.Property(e => e.LastModified).HasColumnName("Last_Modified");

                entity.Property(e => e.Status).HasMaxLength(50);
            });

            modelBuilder.Entity<TableUser>(entity =>
            {
                entity.ToTable("Table_User");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.DateAdded)
                    .HasColumnName("Date_Added")
                    .HasMaxLength(50);

                entity.Property(e => e.DateModified)
                    .HasColumnName("Date_Modified")
                    .HasMaxLength(50);

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.Gender).HasMaxLength(50);

                entity.Property(e => e.OtheName).HasMaxLength(50);

                entity.Property(e => e.PhoneNo).HasMaxLength(50);

                entity.Property(e => e.Surname).HasMaxLength(50);

                entity.Property(e => e.UserName).HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
