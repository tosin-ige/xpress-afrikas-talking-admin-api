﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace Xpress.AfricasTalking.Admin.API.Model
{
    public class UserModel
    {
     
        public string surname{get;set;}
        public string othername { get; set; }
        public string email { get; set; }
        public string phoneno { get; set; }
        public string username { get; set; }
        public string password { get; set; }
       
    }
}
