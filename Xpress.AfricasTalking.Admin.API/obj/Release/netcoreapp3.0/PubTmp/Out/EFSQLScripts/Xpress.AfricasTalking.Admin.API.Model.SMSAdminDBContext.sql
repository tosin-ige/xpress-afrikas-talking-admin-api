﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200515153053_emtiFramework')
BEGIN
    CREATE TABLE [Table_EndPoint] (
        [Id] int NOT NULL,
        [EndPoint] nvarchar(max) NULL,
        [Provider] nvarchar(max) NULL,
        [Status] nvarchar(50) NULL,
        [Date_Added] nvarchar(max) NULL,
        [Last_Modified] nvarchar(max) NULL,
        CONSTRAINT [PK_Table_EndPoint] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200515153053_emtiFramework')
BEGIN
    CREATE TABLE [Table_User] (
        [Id] int NOT NULL,
        [Surname] nvarchar(50) NULL,
        [OtheName] nvarchar(50) NULL,
        [Gender] nvarchar(50) NULL,
        [Email] nvarchar(50) NULL,
        [PhoneNo] nvarchar(50) NULL,
        [UserName] nvarchar(50) NULL,
        [Password] nvarchar(max) NULL,
        [Date_Added] nvarchar(50) NULL,
        [Date_Modified] nvarchar(50) NULL,
        CONSTRAINT [PK_Table_User] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200515153053_emtiFramework')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200515153053_emtiFramework', N'3.1.3');
END;

GO

