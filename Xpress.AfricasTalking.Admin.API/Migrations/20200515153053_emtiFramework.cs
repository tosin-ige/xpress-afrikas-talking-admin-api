﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Xpress.AfricasTalking.Admin.API.Migrations
{
    public partial class emtiFramework : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Table_EndPoint",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    EndPoint = table.Column<string>(nullable: true),
                    Provider = table.Column<string>(nullable: true),
                    Status = table.Column<string>(maxLength: 50, nullable: true),
                    Date_Added = table.Column<string>(nullable: true),
                    Last_Modified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Table_EndPoint", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Table_User",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Surname = table.Column<string>(maxLength: 50, nullable: true),
                    OtheName = table.Column<string>(maxLength: 50, nullable: true),
                    Gender = table.Column<string>(maxLength: 50, nullable: true),
                    Email = table.Column<string>(maxLength: 50, nullable: true),
                    PhoneNo = table.Column<string>(maxLength: 50, nullable: true),
                    UserName = table.Column<string>(maxLength: 50, nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Date_Added = table.Column<string>(maxLength: 50, nullable: true),
                    Date_Modified = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Table_User", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Table_EndPoint");

            migrationBuilder.DropTable(
                name: "Table_User");
        }
    }
}
